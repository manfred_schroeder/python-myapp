# README #

Python application framework using SQLAlchemy and logging

* Python framework
* 0.1

### Setup ###

* Install python (python3), inflect, sqlalchemy
* Install python modules for database
  eg: for mysql/mariadb install python's mysqldb or PyMySQL

* Create a database and start by building models
* Modify program.py to suit your needs

